open Core.Std;;

module T = Time;;
module Ts = Time.Span;;

class timer : Log_f.timer_sexp ->
  object ('a)
    val duration : Ts.t
    val mutable marked_finished : bool
    val name : string
    val of_type : Log_f.sort_of_timer
    val mutable running_meanwhile : Lwt_process.process_none
    val running_when_done : string
    val start_time : T.t
    method private call_on_finish_once : unit
    method mark_finished : unit
    method reset : 'a
    method is_finished : bool
    method name : string
    method of_type : Log_f.sort_of_timer
    method remaining : Ts.t option
    method remaining_str : string
    method run_done : unit
    method ensure_consistency : unit
  end

val notify_end : timer -> unit

(* A cycling set of timers, like pomodro, break, pomodoro, break, pomodoro, long
 * break *)
class cycling : log:Log_f.read_log ->
  object ('a)
    (* Return current timer. Cycles through timers and call final_call, as one finishes *)
    method get : (timer -> unit) -> timer
    (* Change current timer. May change a finished, which would be deleted as
     * get method is called. *)
    method map_current_timer : f:(timer -> timer) -> unit
  end
;;
