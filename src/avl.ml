open Core.Std;;

class ['a] t ?(compare=compare) read_value = object(s)
  val log : 'a = read_value
  val mutable actual = read_value
  method get_log = log
  method private get_actual = actual
  (* Shorthand *)
  method get = s#get_actual
  (* Update actual value, but keep log as-is, giving a new object if we try to
   * change it *)
  method update_log new_log_value = {< log = new_log_value >}
  method private update_actual updated_value =
    actual <- updated_value
  method set = s#update_actual
  (* Turn the actual state to the log one *)
  method turn2log = s#update_actual s#get_log
  (* Gives both values if they differs *)
  method both =
    Option.some_if ((compare log actual) <> 0) (log, actual)

  (* Returning string with all relevant values *)
  method print_both f =
    (* f converts to string *)
    match s#both with
    | None -> f s#get
    | Some (log_value, actual_value) ->
      sprintf "%s (log: %s)" (f actual_value) (f log_value)
end
