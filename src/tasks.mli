open Core.Std;;

type status = Active | Done

class ptask :
  ?num:int ->
  ?done_at:string ->
  ?number_of_pomodoro:int ->
  ?estimation:int ->
  ?short_interruption:int ->
  ?long_interruption:int ->
  ?day:Date.t ->
  ?description:string ->
  name:string ->
  object ('a)
    method description : string option Avl.t
    method done_at : string option Avl.t
    method estimation : int option Avl.t
    method day : Date.t option Avl.t
    method id : int

    method short_interruption : int option Avl.t
    method record_short_interruption : unit

    method long_interruption : int option Avl.t
    (* XXX The type used here is a bit tricky, we mean Timer.timer actually but
    this leads to circular build dependancies *)
    method record_long_interruption : unit

    method is_done : bool
    method long_summary : string
    method mark_done : unit
    method name : string Avl.t
    method num : int option Avl.t

    method number_of_pomodoro : int option Avl.t
    method record_pomodoro : unit

    method short_summary : string
    method status : status Avl.t
    method private summary : long:bool -> string
    method update_with : 'a -> 'a
  end

val get_pending : ptask list -> ptask option
