open Core.Std;;

let () =
  Unix.nice 19 |> ignore;

  (* Get timers with command line arguments, mutable to allow easier update *)
  let log =
    Sys.argv
    |> (function
        | [| _ ; name |] -> new Log_f.read_log name
        | _ -> failwith "Needs exactly one argument, filename of your log file.")
  in

  Lwt_main.run ( Views.mainv ~log () );;
