open Core.Std;;

(* Represents actual states and states read from log file
 * (avl stands for actual_vs_log) *)

(* Create an avl around value. The [cmp_sexp] couple of function maybe be
 * provided as a custom comparator and converter to s-expression,
 * respectively. *)
class ['a] t :
  ?compare:('a -> 'a -> int) ->
  'a ->
  object ('b)
    val mutable actual : 'a
    val log : 'a
    method both : ('a * 'a) option
    method print_both : ('a -> string) -> string
    method get : 'a
    method private get_actual : 'a
    method get_log : 'a
    method set : 'a -> unit
    method turn2log : unit
    method private update_actual : 'a -> unit
    method update_log : 'a -> 'b
  end
