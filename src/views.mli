open Core.Std;;

open LTerm_widget;;

(* Main view *)
val mainv : log:Log_f.read_log -> unit -> unit Lwt.t
