open Core.Std;;

type sort_of_timer =
  | Pomodoro
  | Short_break
  | Long_break
;;

type timer_sexp = {
  sort : sort_of_timer;
  duration : float;
  (* Command to play sound while pomodoro is running *)
  ticking_command : string;
  (* Command to a play sound when pomodoro is finished *)
  ringing_command : string;
  (* Maximum time the ringing command is allowed to run *)
  max_ring_duration : float;
}
type settings_sexp = {
  tick : float;
  timer_cycle : timer_sexp sexp_list
}

type internal_read_log

(* Self refreshing log file *)
class read_log : string -> object
    val mutable internal_read_log : internal_read_log
    method fname : string
    method private irl : internal_read_log
    method ptasks : Tasks.ptask list
    method settings : settings_sexp
  end
;;
